/*
 * Monorail.asm
 *
 *  Created: 10/1/2012 1:15:09 PM
 *   Author: Chris
 * 	wendilim@cse.unsw.edu.au
 */



.include "m64def.inc"

;********************;
; Port Configuration ;
;********************;
;D0-D7 -> PC0-PC7
.equ LCD_DATA_DDR = DDRC
.equ LCD_DATA_PIN = PINC
.equ LCD_DATA_PORT = PORTC

;BE-RS -> PB0-PB3 ;does not effect any other PB pins
.equ LCD_CONTROL_DDR = DDRB
.equ LCD_CONTROL_PIN = PINB
.equ LCD_CONTROL_PORT = PORTB

;MOT -> PB4; does not effect any other pins

; PB0-PB1 -> PD0-PD1 ;interrupt pins

; LED0-1 -> PD2-3 ; does not effect other PD pins
.equ LED_DDR = DDRD
.equ LED_PIN = PIND
.equ LED_PORT = PORTD

; R0-R3 -> PA0-PA3
; C0-C3 -> PA4-PA7

;********************;
; Registers          ;
;********************;

.def temp =r16
.def data =r17
.def del_lo = r18
.def del_hi = r19
.def count = r20


;**********************;
; Registers for keypad ;
;**********************;

.def row = r21
.def col = r22
.def mask = r23
.def temp2 = r24

.equ DEFAULT_SPEED = 153 ;60rps = 255*0.6

.dseg
station_names: .byte 110 ; 10 names of length 10 with 1 backslash
.equ terminator = '\\'
.equ station_name_offset = 11
num_stations: .byte 1
station_gaps: .byte 10
stop_time: .byte 1

.cseg

jmp reset
.org INT0addr ; INT0addr is the address of EXT_INT0
jmp PB0_INT
.org INT1addr ; INT1addr is the address of EXT_INT1
jmp PB1_INT

;****************************************************************************************;
; LCD
;****************************************************************************************;
;********************;
; LCD Constants      ;
;********************;

;LCD protocol control bits
.equ LCD_RS = 3
.equ LCD_RW = 1
.equ LCD_E = 2
;LCD functions
.equ LCD_FUNC_SET = 0b00110000
.equ LCD_DISP_OFF = 0b00001000
.equ LCD_DISP_CLR = 0b00000001
.equ LCD_DISP_ON = 0b00001100
.equ LCD_ENTRY_SET = 0b00000100
.equ LCD_ADDR_SET = 0b10000000
;LCD function bits and constants
.equ LCD_BF = 7
.equ LCD_N = 3
.equ LCD_F = 2
.equ LCD_ID = 1
.equ LCD_S = 0
.equ LCD_C = 1
.equ LCD_B = 0
.equ LCD_LINE1 = 0
.equ LCD_LINE2 = 0x40

;********************;
; LCD Functions      ;
;********************;
;Function lcd_write_com: Write a command to the LCD. The data reg stores the value to be written.
lcd_write_com:
	out LCD_DATA_PORT, data ; set the data port's value up

	;clr temp
	in temp, LCD_CONTROL_PORT
	cbr temp,15

	out LCD_CONTROL_PORT, temp ; RS = 0, RW = 0 for a command write
	nop ; delay to meet timing (Set up time)
	sbi LCD_CONTROL_PORT, LCD_E ; turn on the enable pin
	nop ; delay to meet timing (Enable pulse width)
	nop
	nop
	cbi LCD_CONTROL_PORT, LCD_E ; turn off the enable pin
	nop ; delay to meet timing (Enable cycle time)
	nop
	nop
ret
;Function lcd_write_data: Write a character to the LCD. The data reg stores the value to be written.
lcd_write_data:
	out LCD_DATA_PORT, data ; set the data port's value up

	in temp, LCD_CONTROL_PORT
	cbr temp, 15
	sbr temp, 1 << LCD_RS

	out LCD_CONTROL_PORT, temp ; RS = 1, RW = 0 for a data write
	nop ; delay to meet timing (Set up time)
	sbi LCD_CONTROL_PORT, LCD_E ; turn on the enable pin
	nop ; delay to meet timing (Enable pulse width)
	nop
	nop
	cbi LCD_CONTROL_PORT, LCD_E ; turn off the enable pin
	nop ; delay to meet timing (Enable cycle time)
	nop
	nop
ret
;Function lcd_wait_busy: Read the LCD busy flag until it reads as not busy.
lcd_wait_busy:
	clr temp
	out LCD_DATA_DDR, temp ; Make LCD_DATA_PORT be an input port for now
	out LCD_DATA_PORT, temp

	in temp, LCD_CONTROL_PORT
	cbr temp, 15
	sbr temp, 1 << LCD_RW

	out LCD_CONTROL_PORT, temp ; RS = 0, RW = 1 for a command port read
	busy_loop:
	nop ; delay to meet timing (Set up time / Enable cycle time)
	sbi LCD_CONTROL_PORT, LCD_E ; turn on the enable pin
	nop ; delay to meet timing (Data delay time)
	nop
	nop
	in temp, LCD_DATA_PIN ; read value from LCD
	cbi LCD_CONTROL_PORT, LCD_E ; turn off the enable pin
	sbrc temp, LCD_BF ; if the busy flag is set
	rjmp busy_loop ; repeat command read

	in temp, LCD_CONTROL_PIN
	cbr temp, 15
	;clr temp ; else

	out LCD_CONTROL_PORT, temp ; turn off read mode,
	ser temp
	out LCD_DATA_DDR, temp ; make LCD_DATA_PORT an output port again
ret ; and return

; Function delay: Pass a number in registers r18:r19 to indicate how many microseconds
; must be delayed. Actual delay will be slightly greater (~1.08us*r18:r19). ; r18:r19 are altered in this function.
delay:
	subi del_lo, 1
	sbci del_hi, 0
	nop
	nop
	nop
	nop
	brne delay
ret

.macro delay_macro
	ldi del_lo, low(@0)
	ldi del_hi, high(@0)
	rcall delay
.endmacro

;Function lcd_init Initialisation function for LCD.
lcd_init:
	ser temp
	out LCD_DATA_DDR, temp ; LCD_DATA_PORT, the data port is usually all otuputs
	out LCD_CONTROL_DDR, temp ; LCD_CONTROL_PORT, the control port is always all outputs
	delay_macro 15000 ; delay for > 15ms
	; Function set command with N = 1 and F = 0
	ldi data, LCD_FUNC_SET | (1 << LCD_N)
	rcall lcd_write_com ; 1st Function set command with 2 lines and 5*7 font
	delay_macro 4100 ; delay for > 4.1ms
	rcall lcd_write_com ; 2nd Function set command with 2 lines and 5*7 font
	delay_macro 100; delay for > 100us
	rcall lcd_write_com ; 3rd Function set command with 2 lines and 5*7 font
	rcall lcd_write_com ; Final Function set command with 2 lines and 5*7 font
	rcall lcd_wait_busy ; Wait until the LCD is ready
	ldi data, LCD_DISP_OFF
	rcall lcd_write_com ; Turn Display off
	rcall lcd_wait_busy ; Wait until the LCD is ready
	ldi data, LCD_DISP_CLR
	rcall lcd_write_com ; Clear Display
	rcall lcd_wait_busy ; Wait until the LCD is ready
	; Entry set command with I/D = 1 and S = 0
	ldi data, LCD_ENTRY_SET | (1 << LCD_ID)
	rcall lcd_write_com ; Set Entry mode: Increment = yes and Shift = no
	rcall lcd_wait_busy ; Wait until the LCD is ready
	; Display on command with C = 0 and B = 1
	ldi data, LCD_DISP_ON | (1 << LCD_C)
	rcall lcd_write_com ; Turn Display on with a cursor that doesn't blink
ret

;********************;
; MY LCD FUNCTIONS   ;
;********************;

lcd_goto_line2:
	push data
	ldi data, LCD_ADDR_SET | LCD_LINE2
	rcall lcd_wait_busy
	rcall lcd_write_com
	pop data
ret

lcd_print_data:
        rcall lcd_wait_busy
        rcall lcd_write_data
ret

lcd_clear_screen:
	push data
	ldi data, LCD_DISP_CLR
	rcall lcd_wait_busy
	rcall lcd_write_com
	pop data
ret

.macro print_string_from_memory ;takes in string and terminating backslash
		push data
        ldi ZL, low(@0 << 1)        ; point Y at the string
        ldi ZH, high(@0 << 1)       ; recall that we must multiply any Program code label addressby 2 to get the correct location
		rcall lcd_psfm
		pop data
.endmacro

lcd_psfm:
        lpm data, Z+                    ; read a character from the string
		cpi data, terminator
		breq end_lcd_psfm
        rcall lcd_print_data    ; write the character to the screen
        brne lcd_psfm                  ; loop again if there are more characters
end_lcd_psfm:
ret

.macro print_string_from_dmemory ;takes in string and length
		push data
        ldi ZL, low(@0)        ; point Z at the string
        ldi ZH, high(@0)       ; recall that we must multiply any Program code label addressby 2 to get the correct location
		rcall lcd_psfdm
		pop data
.endmacro

lcd_psfdm:
        ld data, Z+                    ; read a character from the string
		cpi data, terminator
		breq end_lcd_psfdm
        rcall lcd_print_data    ; write the character to the screen
        brne lcd_psfdm                  ; loop again if there are more characters
end_lcd_psfdm:
ret


.macro print_station_name ;takes in id stored in register
		push data
		push temp
        ldi ZL, low(station_names)        ; point Z at the string
        ldi ZH, high(station_names)       ; recall that we must multiply any Program code label addressby 2 to get the correct location
	lpsn:
		cpi @0, 1
		breq print_lcd_psfdm
		ldi temp, station_name_offset
		add zl, temp
		ldi temp, 0
		adc zh, temp
		dec @0
		rjmp lpsn

	print_lcd_psfdm:
		rcall lcd_psfdm
		pop temp
		pop data
.endmacro

;****************************************************************************************;
; LED BLINK
;****************************************************************************************;
led_on:
	push temp
	in temp, LED_PORT
	sbr temp, 12 ; this is done so only alters bit 2 and 3
	out LED_PORT, temp
	pop temp
ret
led_off:
	push temp
	push data
	in temp, LED_PORT
	cbr temp, 12 ; this is done so only alters bit 2 and 3
	out LED_PORT, temp
	pop data
	pop temp
ret

;****************************************************************************************;
; Motor
;****************************************************************************************;
motor_on:
	push temp
	ldi temp, DEFAULT_SPEED
	out OCR0, temp
	pop temp
ret
motor_off:
	push temp
	ldi temp, 0
	out OCR0, temp
	pop temp
ret


;****************************************************************************************;
; Push Button Interrupts
;****************************************************************************************;

PB0_INT:
rcall motor_on
rcall led_off
reti


PB1_INT:
rcall motor_off
rcall led_on
reti


;****************************************************************************************;
; KEYPAD
;****************************************************************************************;
.equ KEYDIR = 0xF0
.equ INITCOLMASK = 0xEF
.equ INITROWMASK = 0x01
.equ ROWMASK = 0x0F

; keypad ports

.equ KEYPORT = PORTA
.equ KEYDDR = DDRA
.equ KEYPIN = PINA

; called after RESET
key_init:
ldi temp, KEYDIR	; columns are outputs, rows are inputs
out KEYDDR, temp
ser temp
ret

colloop:
	out KEYPORT, mask ; set column to mask value
	ldi temp, 0xFF ; implement delay so hardware can stabilize

keypad_delay:
	dec temp
	brne keypad_delay

wait_for_key:
	in temp, KEYPIN ; read PORTA
	andi temp, ROWMASK ; read only the row bits
	cpi temp, 0xF ; check if any rows are grounded
	breq next_col ; if not go to the next column

wait_key_release:
	in temp2, KEYPIN
	andi temp2, ROWMASK
	cpi temp2, 0xF
	brne wait_key_release

ldi mask, INITROWMASK ; initialise the row check
clr row ; initial row

row_loop:
	mov temp2, temp
	and temp2, mask ; check masked bit
	brne skip_conv ; if the result is non-zero, we need to look again
	rcall convert ; if bit is clear, convert the bitcode
	jmp get_num_input ; go back to where it is called, probably will fail

skip_conv:
	inc row ; else move to the next row
	lsl mask ; shift teh mask to the next bit
	jmp row_loop

next_col:
	cpi col, 3 ; check if we’re on the last column
	breq get_num_input ; if so, no buttons were pushed


sec 		; else shift the column mask:
			; We must set the carry bit
rol mask 	; and then rotate left by a bit,
			; shifting the carry into
			; bit zero. We need this to make
			; sure all the rows have
			; pull-up resistors
inc col 	; increment column value
jmp colloop ; and check the next column



convert:
	cpi col, 3 ; if column is 3 we have a letter
	breq letters
	cpi row, 3 ; if row is 3 we have a symbol or 0
	breq symbols
	mov temp, row ; otherwise we have a number (1-9)
	lsl temp ; temp = row * 2
	add temp, row ; temp = row * 3
	add temp, col ; add the column address
	; to get the offset from 1
	inc temp ; add 1. Value of switch is
	; row*3 + col + 1.
	; now print to screen
	ldi data, '0'
	add data, temp
	mov r0, data
	jmp convert_end

; can modify this to print Q and Z
letters:
	cpi row, 3 ; check if D was pressed
	breq save_number
	ldi temp, 0xA
	add temp, row ; increment from 0xA by the row value
	jmp convert_end

symbols:
	cpi col, 0 ; check if we have a star
	breq star
	cpi col, 1 ; or if we have zero
	breq zero
	ldi temp, 0xF ; we'll output 0xF for hash
	ldi data, '#'
	jmp convert_end

star:
	ldi temp, 0xE
	ldi data, '*'
	jmp convert_end

zero:
	clr temp
	ldi data, '0'
	mov r0, data
	jmp convert_end

save_number:
	mov temp, r0 ; move the number from r0 to temp
	ldi data, '0'
	add data, temp
	jmp convert_end

convert_end:
	rcall lcd_wait_busy
	rcall lcd_write_data
ret

;*****************************************************************************************
; LOGIC CODE
;*****************************************************************************************
.macro temp_delay
	delay_macro 65500
	delay_macro 65500
	delay_macro 65500
	delay_macro 65500
	delay_macro 65500
	delay_macro 65500
	delay_macro 65500
	delay_macro 65500
.endmacro

get_num_input: ;store result in temp ; WENDIS FUNCTION TO GET INTEGER INPUT 1-10
	ldi mask, INITCOLMASK ; initial column mask
	clr col ; initial column
	jmp colloop
ret

get_num_stations:
	rcall lcd_clear_screen
	print_string_from_memory config1

	ldi xl, low(num_stations)
	ldi xh, high(num_stations)

	rcall get_num_input

	st x, temp
ret

.macro get_ns
	ldi xl, low(num_stations)
	ldi xh, high(num_stations)
	ld temp, x
.endmacro

get_station_names:
	clr count
	config_loop:
	temp_delay
	rcall lcd_clear_screen
	print_string_from_memory config2
	ldi data, '1'
	add data, count
	rcall lcd_print_data
	ldi data, ':'
	rcall lcd_print_data
	rcall lcd_goto_line2
	
	;WENDI FUNCTION FOR TEXT INPUT SHOULD SAVE THE NAMES


	inc count
	get_ns
	cp count, temp
	brne config_loop
ret

get_station_gaps:
clr count

ldi yl, low(station_gaps)
ldi yh, high(station_gaps)

config_loop1:
	temp_delay
	rcall lcd_clear_screen
	print_string_from_memory config3
	rcall lcd_goto_line2
	ldi data, '1'
	add data, count
	rcall lcd_print_data
	ldi data, '&'
	rcall lcd_print_data

	get_ns
	dec temp
	cp count, temp
	breq lpd1
	ldi data, '2'
	add data, count
	rjmp lpd
	lpd1: ldi data,'1'
	lpd:rcall lcd_print_data

	rcall get_num_input ;magic function

	st y+, temp

	inc count
	get_ns
	cp count, temp
brne config_loop1

ret

get_stop_time:
	rcall lcd_clear_screen
	print_string_from_memory config4

	ldi xl, low(stop_time)
	ldi xh, high(stop_time)

	rcall get_num_input ;magic function

	st x, temp
ret

start_configuration:

	print_string_from_memory welcome

	temp_delay

	rcall get_num_stations

	temp_delay

	rcall get_station_names

	temp_delay

	rcall get_station_gaps

	temp_delay

	rcall get_stop_time

ret

;********************;
; Reset              ;
;********************;
reset:
	;initializing stack pointers
	ldi temp, low(RAMEND)
	out SPL, temp
	ldi temp, high(RAMEND)
	out SPH, temp
	;initializing lcd
	rcall lcd_init
	rcall key_init

	;set up button interrupts
	ldi temp, (2 << ISC10) | (2 << ISC00) ;setting the interrupts for falling edge
	sts EICRA, temp                       ;storing them into EICRA
	in temp, EIMSK                        ;taking the values inside the EIMSK
	ori temp, (1<<INT0) | (1<<INT1)       ; oring the values with INT0 and INT1
	out EIMSK, temp                       ; enabling interrput0 and interrupt1
	sei                        ; enabling the global interrupt..(MUST)

	;set up some code segment variables
	welcome: .db "Monorail Setup!\\"
	config1: .db "Num Stations:\\"
	config2: .db "Name Station \\"
	config3: .db "Time between\\"
	config4: .db "Stop time:\\"

	;set up port with led and motor to be output
	ldi temp, 0b00011100
	out LED_DDR, temp

	;set up pwm
	ldi temp, (0<<FOC0) | (1<<WGM00)|(1<<COM01) |(0<<COM00) | (0<<WGM01)|(1<<CS02)|(1<<CS01)|(1<<CS00)
	out TCCR0, temp


	;test code will be deleted
	ldi xl, low(station_names)
	ldi xh, high(station_names)
	ldi temp, 'a'
	st x+, temp
	st x+, temp
	st x+, temp
	st x+, temp
	st x+, temp
	st x+, temp
	st x+, temp
	st x+, temp
	st x+, temp
	st x+, temp
	ldi temp, terminator
	st x+, temp
	ldi temp, 'b'
	st x+, temp
	st x+, temp
	st x+, temp
	st x+, temp
	st x+, temp
	ldi temp, terminator
	st x+, temp

jmp main

;********************;
; Main               ;
;********************;

main:
	rcall start_configuration

	temp_delay
	rcall lcd_clear_screen
	ldi count, 1
	print_station_name count
	temp_delay
	rcall lcd_clear_screen
	ldi count, 2
	print_station_name count

end: rjmp end                                        ; infinite loop
