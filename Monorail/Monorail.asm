/*
 *   Monorail.asm
 *
 *   Created: 10/1/2012 1:15:09 PM
 *   Author: Chris(z3374974) & Wendi(z#######)
 *
 */



.include "m64def.inc"

;;;;;;;;;;;;;;;;;
;; PORT CONFIG ;;
;;;;;;;;;;;;;;;;;

;***************;
; LCD           ;
;***************;
;D0-D7 -> PC0-PC7
.equ LCD_DATA_DDR = DDRC
.equ LCD_DATA_PIN = PINC
.equ LCD_DATA_PORT = PORTC

;BE-RS -> PB0-PB3 ;does not effect any other PB pins
.equ LCD_CONTROL_DDR = DDRB
.equ LCD_CONTROL_PIN = PINB
.equ LCD_CONTROL_PORT = PORTB

;***************;
; MOTOR         ;
;***************;
;MOT -> PB4;
.equ MOTOR_PORT = OCR0

;***************;
; PUSH BUTTON   ;
;***************;
;PB0 -> PD0
;PB1 -> PD1

;***************;
; LED           ;
;***************;
; LED0 -> PD2
; LED1 -> PD3
.equ LED_DDR = DDRD
.equ LED_PIN = PIND
.equ LED_PORT = PORTD

;***************;
; KEYPAD        ;
;***************;
; R0-R3 -> PA0-PA3
; C0-C3 -> PA4-PA7
.equ KEYPORT = PORTA
.equ KEYDDR = DDRA
.equ KEYPIN = PINA

;;;;;;;;;;;;;;;
;; CONSTANTS ;;
;;;;;;;;;;;;;;;

;*************;
; KEYPAD      ;
;*************;
.equ KEYDIR = 0xF0
.equ INITCOLMASK = 0xEF
.equ INITROWMASK = 0x01
.equ ROWMASK = 0x0F

;*************;
; MOTOR       ;
;*************;
.equ DEFAULT_SPEED = 153 ;60rps = 255*0.6

;*************;
; LCD         ;
;*************;
;Control bits
.equ LCD_RS = 3
.equ LCD_RW = 1
.equ LCD_E = 2
;Functions
.equ LCD_FUNC_SET = 0b00110000
.equ LCD_DISP_OFF = 0b00001000
.equ LCD_DISP_CLR = 0b00000001
.equ LCD_DISP_ON = 0b00001100
.equ LCD_ENTRY_SET = 0b00000100
.equ LCD_ADDR_SET = 0b10000000
.equ LCD_BACK = 0b00010000
;Function bits and constants
.equ LCD_BF = 7
.equ LCD_N = 3
.equ LCD_F = 2
.equ LCD_ID = 1
.equ LCD_S = 0
.equ LCD_C = 1
.equ LCD_B = 0
.equ LCD_LINE1 = 0
.equ LCD_LINE2 = 0x40

;*************;
; PROGRAM     ;
;*************;
.equ terminator = '\\'
.equ station_name_offset = 11

;;;;;;;;;;;;;;;
;; REGISTERS ;;
;;;;;;;;;;;;;;;

.def temp =r16
.def data =r17
.def del_lo = r18
.def del_hi = r19
.def count = r20
.def next = r25
.def q_next = r13
.def seconds = r15
.def q_seconds= r14
.def row = r21
.def col = r22
.def mask = r23
.def temp2 = r24

;;;;;;;;;;;;;;;;;;
;; DATA SEGMENT ;;
;;;;;;;;;;;;;;;;;;
.dseg
station_names: .byte 110 ; 10 names of length 10 with 1 backslash
num_stations: .byte 1
station_gaps: .byte 10
stop_time: .byte 1

;simulation variables
led_blink: .byte 1
led_blink_state: .byte 1
simulating: .byte 1
stopping: .byte 1
emergency: .byte 1

;;;;;;;;;;;;;;;;;;
;; CODE SEGMENT ;;
;;;;;;;;;;;;;;;;;;
.cseg
;;;;;;;;;;;;;;;;;;;;;;
;; INTERRUPT VECTOR ;;
;;;;;;;;;;;;;;;;;;;;;;
jmp reset
jmp PB0_INT
jmp PB1_INT
jmp default
jmp default
jmp default
jmp default
jmp default
jmp default
jmp default
jmp default
jmp default
jmp timer1
jmp default
jmp default
jmp default
default:reti


;;;;;;;;;;;;
;; MACROS ;;
;;;;;;;;;;;;

.macro get_data
		push zl
		push zh

        ldi zl, low(@0)
        ldi zh, high(@0)
        ld temp, z
		pop zh
		pop zl
.endmacro

.macro save_data
		push zl
		push zh

        ldi zl, low(@0)
        ldi zh, high(@0)
        st z, temp

		pop zh
		pop zl
.endmacro


.macro get_ns
		push xl
		push xh

        ldi xl, low(num_stations)
        ldi xh, high(num_stations)
        ld temp, x

		pop xh
		pop xl
.endmacro

.macro get_st
		push xl
		push xh

        ldi xl, low(stop_time)
        ldi xh, high(stop_time)
        ld temp, x

		pop xh
		pop xl
.endmacro

.macro get_sg ;offselt
		push xl
		push xh

        ldi xL, low(station_gaps)
        ldi xH, high(station_gaps)


		add xl, @0
		clr r0
		adc xh, r0

        ld @1, x

		pop xh
		pop xl
.endmacro

.macro start_led_blink
		ldi temp, 1
		save_data led_blink
.endmacro

.macro stop_led_blink
	ldi temp, 0
	save_data led_blink
	rcall led_off
.endmacro

.macro delay_macro
    ldi del_lo, low(@0)
    ldi del_hi, high(@0)
    rcall delay
.endmacro

.macro intToLCD
	push data
	ldi data, '0'
	add data, @0
	rcall lcd_wait_busy
	rcall lcd_write_data
	pop data
.endmacro

.macro print_string_from_memory ;takes in string and terminating backslash
        push data
		push zl
		push zh

        ldi zL, low(@0 << 1)        ; point Y at the string
        ldi zH, high(@0 << 1)       ; recall that we must multiply any Program code label addressby 2 to get the correct location
        rcall lcd_psfm

		pop zh
		pop zl
        pop data
.endmacro

.macro print_string_from_dmemory ;takes in string
        push data
		push zl
		push zh

        ldi zL, low(@0)        ; point Z at the string
        ldi zH, high(@0)       ; recall that we must multiply any Program code label addressby 2 to get the correct location
		rcall lcd_psfdm

		pop zh
		pop zl
		pop data
.endmacro

.macro print_station_name ;takes in id stored in register usually count
        push data
        push temp
		push temp2

		push zl
		push zh

        ldi ZL, low(station_names)        ; point Z at the string
        ldi ZH, high(station_names)       ; recall that we must multiply any Program code label addressby 2 to get the correct location

		mov temp, @0
		ldi temp2, station_name_offset
		mul temp, temp2
		add Zl, r0
		adc Zh, r1


        rcall lcd_psfdm

		pop zh
        pop zl

		pop temp2
        pop temp
        pop data
.endmacro

.macro temp_delay
        delay_macro 65500
        delay_macro 65500
        delay_macro 65500
        delay_macro 65500
        delay_macro 65500
        delay_macro 65500
        delay_macro 65500
        delay_macro 65500
.endmacro

;;;;;;;;;;;;;
;; Timer 1 ;;
;;;;;;;;;;;;;
;****************************************************************************************
; Timer: Start
;****************************************************************************************
Timer1:
        in r28, SREG
        push r28
		push temp

		ldi temp, 4
		cp q_seconds, temp
		brge second_branch
			inc q_seconds
			rjmp exit
		second_branch:
			inc seconds

				get_data emergency
				cpi temp, 1
				breq dont_decrease
				dec next
				dont_decrease:
			clr q_seconds
			rjmp exit

exit:


	get_data emergency
	cpi temp, 1
	brne no_emergency
		rcall led_flip
		rcall motor_off
		rjmp exit1

	no_emergency:

	get_data led_blink
	cpi temp,0
	breq end_blink_check
	rcall led_flip
		cpi next, 1
		brne end_blink_check
			cp q_seconds, q_next
			brne end_blink_check
				rcall station_stop_end

	end_blink_check:


	 get_data simulating
	 cpi temp, 1
	 brne exit1
	 ; just cause of emergency
		rcall led_off
		rcall motor_on
	;
	    cpi next, 1
		brne exit1
			cp q_seconds, q_next
			brne exit1
				get_data stopping
				cpi temp, 1
				brne contin
					rcall station_stop
					rjmp exit1
				contin:
					inc count
					rcall next_station


exit1:

		pop temp
        pop r28                 ; Epilogue starts;
        out SREG, r28           ; Restore all conflict registers from the stack.

        reti                    ; Return from the interrupt.


;****************************************************************************************
; Timer: End
;****************************************************************************************

;;;;;;;;;;;
;; RESET ;;
;;;;;;;;;;;
reset:
        ;initializing stack pointers
        ldi temp, low(RAMEND)
        out SPL, temp
        ldi temp, high(RAMEND)
        out SPH, temp
        ;initializing lcd
        rcall lcd_init
        rcall key_init

        ;set up button interrupts
        ldi temp, (2 << ISC10) | (2 << ISC00) ;setting the interrupts for falling edge
        sts EICRA, temp                       ;storing them into EICRA
        in temp, EIMSK                        ;taking the values inside the EIMSK
        ori temp, (1<<INT0) | (1<<INT1)       ; oring the values with INT0 and INT1
        out EIMSK, temp                       ; enabling interrput0 and interrupt1


		;set up timer1 compare interrupt every 1 second is amazing balls
		clr seconds


		ldi temp, high(1799) ; will go every quarter second
		out ocr1ah, temp
		ldi temp, low(1799)
		out ocr1al, temp

		ldi temp, (1 << OCIE1A)
		out TIMSK, temp
		clr temp
		out TCCR1A, temp
		ldi temp, (1<< WGM12)|(1<<CS10) | (1<<CS12)
		out TCCR1B, temp


        sei                        ; enabling the global interrupt..(MUST)

        ;set up some code segment variables
        welcome: .db "Monorail Setup!\\"
        config1: .db "Num Stations:\\"
        config2: .db "Name Station \\"
        config3: .db "Time between\\"
        config4: .db "Stop time:\\"
		msg_start: .db "Configuring...\\"
		msg_next: .db "Next Stop: \\"
		msg_stopped: .db "Stopped at:\\"
        ;set up port with led and motor to be output
        ldi temp, 0b00011100
        out LED_DDR, temp

        ;set up pwm
        ldi temp, (0<<FOC0) | (1<<WGM00)|(1<<COM01) |(0<<COM00) |(0<<WGM01)|(1<<CS02)|(1<<CS01)|(1<<CS00)
        out TCCR0, temp


		;
		ldi temp, 0
		save_data led_blink
		save_data led_blink_state
		save_data simulating
		save_data stopping
		save_data emergency
jmp main

;;;;;;;;;;
;; MAIN ;;
;;;;;;;;;;

main:
		rcall start_configuration

		rcall lcd_clear_screen
		print_string_from_memory msg_start
		temp_delay

		ldi temp, 1
		save_data simulating
		clr count
		rcall next_station

		scan_hash:
		rcall emergency_stop
		rjmp scan_hash
end: rjmp end

;;;;;;;;;;;;;;;;
;; LOGIC CODE ;;
;;;;;;;;;;;;;;;;
;****************************************************************************************
; Logic: Station Stop
;****************************************************************************************
station_stop:
	ldi temp, 0
	save_data simulating

	rcall motor_off
	start_led_blink
	rcall lcd_clear_screen
	print_string_from_memory msg_stopped
	rcall lcd_goto_line2

	mov r9, count
	inc r9
	get_ns
		cp r9, temp
		brne hbas
		clr r9

		hbas:
	print_station_name r9

	get_st
	mov next, temp
	mov q_next, q_seconds

ret
;****************************************************************************************
; Logic: End Station Stop
;****************************************************************************************
;****************************************************************************************
; Logic: Station Stop End
;****************************************************************************************
station_stop_end:
	ldi temp, 1
	save_data simulating
	ldi temp, 0
	save_data stopping
	rcall motor_on
	stop_led_blink
	rcall lcd_clear_screen


ret
;****************************************************************************************
; Logic: End Station Stop End
;****************************************************************************************
;****************************************************************************************
; Logic: Next Station
;****************************************************************************************
next_station: ;uses loads gap of count to next and prints next:station(count+1)
	get_ns
	cp count, temp
	brne hbb
	clr count

	hbb:

	mov r9, count
	inc r9

	get_ns
	cp r9, temp
	brne hba
	clr r9

	hba:
	rcall lcd_clear_screen
	print_string_from_memory msg_next
	rcall lcd_goto_line2
	print_station_name r9

	get_sg count, next
	;ldi data, '0'
	;add data, next
	;rcall lcd_wait_busy
	;rcall lcd_print_data
	mov q_next, q_seconds

	rcall motor_on

ret
;****************************************************************************************
; Logic: End Next Station
;****************************************************************************************

;;;;;;;;;;;;;;;;;;;
;; CONFIGURATION ;;
;;;;;;;;;;;;;;;;;;;
;****************************************************************************************
; Configuration: Start
;****************************************************************************************
start_configuration:

        print_string_from_memory welcome

        temp_delay

        rcall get_num_stations

        rcall get_station_names

        rcall get_station_gaps

        rcall get_stop_time

ret
;****************************************************************************************
; Configuration: End of Start
;****************************************************************************************
;****************************************************************************************
; Configuration: Get Number of Stations
;****************************************************************************************
get_num_stations:
	push xl
	push xh

        rcall lcd_clear_screen
        print_string_from_memory config1

        ldi xl, low(num_stations)
        ldi xh, high(num_stations)

        rcall get_num_input

        st x, r4
	pop xh
	pop xl
ret
;****************************************************************************************
; Configuration: End of Get Number of stations
;****************************************************************************************
;****************************************************************************************
; Configuration: Get Station Names
;****************************************************************************************
get_station_names:
		push xl
		push xh
        clr count
        config_loop:
        temp_delay
        rcall lcd_clear_screen
        print_string_from_memory config2
        ldi temp, 1
        add temp, count
		rcall regIntToLCD
        ldi data, ':'
        rcall lcd_print_data
        rcall lcd_goto_line2


		ldi xl, low(station_names)
		ldi xh, high(station_names)

		mov temp, count
		ldi temp2, station_name_offset
		mul temp, temp2
		add xl, r0
		adc xh, r1

        rcall get_letter_input

        inc count
        get_ns
        cp count, temp
        brne config_loop
		pop xh
		pop xl
ret

;****************************************************************************************
; Configuration: End Get Station Names
;****************************************************************************************
;****************************************************************************************
; Configuration: Get Station Gaps
;****************************************************************************************
get_station_gaps:

push zl
push zh

clr count

ldi zl, low(station_gaps)
ldi zh, high(station_gaps)

config_loop1:
        rcall lcd_clear_screen
        print_string_from_memory config3
        rcall lcd_goto_line2
        ldi temp, 1
        add temp, count
		rcall regIntToLCD
        ldi data, '&'
        rcall lcd_print_data

        get_ns
        dec temp
        cp count, temp
        breq lpd1
        ldi temp, 2
        add temp, count
		rcall regIntToLCD
        rjmp lpd
        lpd1:
		ldi data,'1'
		rcall lcd_print_data
        lpd:

        ldi data, ':'
        rcall lcd_print_data

        rcall get_num_input ;magic function

        st z+, r4
        inc count
        get_ns
        cp count, temp
brne config_loop1

pop zh
pop zl
ret

;****************************************************************************************
; Configuration: End Get Station Gaps
;****************************************************************************************
;****************************************************************************************
; Configuration: Get Stop Time
;****************************************************************************************
get_stop_time:
		push xl
		push xh

        rcall lcd_clear_screen
        print_string_from_memory config4

        ldi xl, low(stop_time)
        ldi xh, high(stop_time)

        rcall get_num_input

        st x, r4

		pop xh
		pop xl
ret
;****************************************************************************************
; Configuration: End Get Stop Time
;****************************************************************************************



;;;;;;;;;
;; LED ;;
;;;;;;;;;
;****************************************************************************************
; LED: On
;****************************************************************************************
led_on:
        push temp
        in temp, LED_PORT
        sbr temp, 12 ; this is done so only alters bit 2 and 3
        out LED_PORT, temp
		ldi temp, 1
		save_data led_blink_state
        pop temp
ret
;****************************************************************************************
; LED: End of On
;****************************************************************************************
;****************************************************************************************
; LED: Flip
;****************************************************************************************
led_flip:
	push temp

	get_data led_blink_state

	cpi temp, 1
	breq led_flip_off
		rcall led_on
		rjmp end_led_flip

	led_flip_off:
		rcall led_off

	end_led_flip:
	pop temp
ret
;****************************************************************************************
; LED: End of Flip
;****************************************************************************************
;****************************************************************************************
; LED: Off
;****************************************************************************************
led_off:
        push temp
        push data
        in temp, LED_PORT
        cbr temp, 12 ; this is done so only alters bit 2 and 3
        out LED_PORT, temp
		ldi temp, 0
		save_data led_blink_state
        pop data
        pop temp
ret
;****************************************************************************************
; LED: End of Off
;****************************************************************************************



;;;;;;;;;;;
;; MOTOR ;;
;;;;;;;;;;;
;****************************************************************************************
; Motor: Functions
;****************************************************************************************
motor_on:
        push temp
        ldi temp, DEFAULT_SPEED
        out MOTOR_PORT, temp
        pop temp
ret
motor_off:
        push temp
        ldi temp, 0
        out MOTOR_PORT, temp
        pop temp
ret
;****************************************************************************************
; Motor: End Functions
;****************************************************************************************


;;;;;;;;;;;;;;;;;
;; PUSH BUTTON ;;
;;;;;;;;;;;;;;;;;
;****************************************************************************************
; Push Button: Interrupts
;****************************************************************************************

PB0_INT:
	ldi temp, 1
	save_data stopping

reti


PB1_INT:
	ldi temp, 1
	save_data stopping

reti

;****************************************************************************************
; Push Button: End Interrupts
;****************************************************************************************



;;;;;;;;;;;;
;; KEYPAD ;;
;;;;;;;;;;;;
;*****************************************************************************************
; Keypad: Reset
;*****************************************************************************************
key_init:
ldi temp, KEYDIR        ; columns are outputs, rows are inputs
out KEYDDR, temp
ser temp
ret
;*****************************************************************************************
; Keypad: End Reset
;*****************************************************************************************
;*****************************************************************************************
; Keypad: Get # Input
;*****************************************************************************************
emergency_stop: ;store result in temp ; WENDIS FUNCTION TO GET INTEGER INPUT 1-10
        ldi mask, INITCOLMASK ; initial column mask
        clr col ; initial column
		clr r4

colloop3:
        out KEYPORT, mask ; set column to mask value
        ldi temp, 0xFF ; implement delay so hardware can stabilize

keypad_delay3:
        dec temp
        brne keypad_delay3

wait_for_key3:
        in temp, KEYPIN ; read PORTA
        andi temp, ROWMASK ; read only the row bits
        cpi temp, 0xF ; check if any rows are grounded
        breq next_col3 ; if not go to the next column

wait_key_release3:
        in temp2, KEYPIN
        andi temp2, ROWMASK
        cpi temp2, 0xF
        brne wait_key_release3

ldi mask, INITROWMASK ; initialise the row check
clr row ; initial row

row_loop3:
        mov temp2, temp
        and temp2, mask ; check masked bit
        brne skip_conv3 ; if the result is non-zero, we need to look again
        rcall convert3 ; if bit is clear, convert the bitcode
        ret ; go back to where it is called, probably will fail

skip_conv3:
        inc row ; else move to the next row
        lsl mask ; shift teh mask to the next bit
        jmp row_loop3

next_col3:
        cpi col, 3 ; check if we are on the last column
        breq emergency_stop ; if so, no buttons were pushed


sec             ; else shift the column mask:
                ; We must set the carry bit
rol mask        ; and then rotate left by a bit,
                ; shifting the carry into
                ; bit zero. We need this to make
                ; sure all the rows have
                ; pull-up resistors
inc col         ; increment column value
jmp colloop3 ; and check the next column

convert3:
	cpi col, 2
	brne emergency_stop
	cpi row, 3
	brne emergency_stop

	get_data emergency
	ldi temp2, 1
	eor temp, temp2
	save_data emergency
ret

;*****************************************************************************************
; Keypad: End Get # Input
;*****************************************************************************************
;*****************************************************************************************
; Keypad: Character Input Helpers
;*****************************************************************************************
.macro find_letter_3 ;@0
        ldi temp, @0
		rcall find_letter_3_loop
.endmacro

find_letter_3_loop:
		add temp, r5
		inc r5
		mov temp2, r5
		cpi temp2, 3
		brne a_3
        clr r5
		a_3:
		mov r4, temp
ret

.macro find_letter_4 ;@0
        ldi temp, @0
		rcall find_letter_4_loop
.endmacro

find_letter_4_loop:
		add temp, r5
		inc r5
		mov temp2, r5
		cpi temp2, 4
		brne a_4
        clr r5
		a_4:
		mov r4, temp
ret
;*****************************************************************************************
; Keypad: End Character Input Helpers
;*****************************************************************************************
;*****************************************************************************************
; Keypad: Get Character Input
;*****************************************************************************************
get_letter_input:
    clr r4
	clr r5
	clr r8

get_letter_begin:
        ldi mask, INITCOLMASK
        clr col

colloop2:
        out KEYPORT, mask ; set column to mask value
        ldi temp, 0xFF ; implement delay so hardware can stabilize

keypad_delay2:
        dec temp
        brne keypad_delay2

wait_for_key2:
        in temp, KEYPIN ; read PORTA
        andi temp, ROWMASK ; read only the row bits
        cpi temp, 0xF ; check if any rows are grounded
        breq next_col2; if not go to the next column

wait_key_release2:
        in temp2, KEYPIN
        andi temp2, ROWMASK
        cpi temp2, 0xF
        brne wait_key_release2

ldi mask, INITROWMASK   ; initialise the row check
clr row                 ; initial row

row_loop2:
        mov temp2, temp
        and temp2, mask ; check masked bit
        brne skip_conv2 ; if the result is non-zero, we need to look again
        rcall convert2  ; if bit is clear, convert the bitcode
        ret             ; go back to where it is called, probably will fail

skip_conv2:
        inc row         ; else move to the next row
        lsl mask        ; shift teh mask to the next bit
        jmp row_loop2

next_col2:
        cpi col, 3      ; check if we are on the last column
        breq get_letter_begin ; if so, no buttons were pushed


sec             ; else shift the column mask:
                ; We must set the carry bit
rol mask        ; and then rotate left by a bit,
                ; shifting the carry into
                ; bit zero. We need this to make
                ; sure all the rows have
                ; pull-up resistors
inc col         ; increment column value
jmp colloop2    ; and check the next column

step_up3:
jmp get_letter_begin

convert2:

        cpi col, 3      ; if column is 3 we have a letter
        breq letters2
        cpi row, 3      ; if row is 3 we have a symbol or 0
        breq print_space
        mov temp, row   ; otherwise we have a number (1-9)
        lsl temp        ; temp = row * 2
        add temp, row   ; temp = row * 3
        add temp, col   ; add the column address
                        ; to get the offset from 1
        inc temp        ; add 1. Value of switch is
                        ; row*3 + col + 1.
                        ; now print to screen

        cpi temp, 1     ; assume no input
        breq get_letter_begin
        cpi temp, 2     ; print a
        breq print_a
        cpi temp, 3     ; print d
        breq print_d
        cpi temp, 4     ; print g
        breq print_g
        cpi temp, 5     ; print j
        breq print_j
        cpi temp, 6     ; print m
        breq print_m
        cpi temp, 7     ; print p
        breq print_p
        cpi temp, 8     ; print t
        breq print_t

print_w:                ; if all fails:
        find_letter_4 'W' ;WXYZ
        jmp convert_end_ctd2

print_space:
        ldi temp, ' '
		mov r4, temp
        jmp convert_end_ctd2

print_a:
		find_letter_3 'A' ;ABC
        jmp convert_end_ctd2

print_d:
        find_letter_3 'D' ;DEF
        jmp convert_end_ctd2

print_g:
        find_letter_3 'G' ;GHI
        jmp convert_end_ctd2

print_j:
        find_letter_3 'J' ;JKL
        jmp convert_end_ctd2

print_m:
        find_letter_3 'M' ;MNO
        jmp convert_end_ctd2

print_p:
        find_letter_4 'P' ;PQRS
        jmp convert_end_ctd2

print_t:
		find_letter_3 'T' ;TUV
        jmp convert_end_ctd2


step_up2:
jmp step_up3


letters2:
        cpi row, 2      ; check if C was pressed to store
        breq store_letter
        cpi row, 3      ; check if D was pressed
        breq save_name
        jmp step_up2    ; if not D, just wait for more input

store_letter:
        clr r5
        mov temp, r4
        st x+, temp
		clr r8
        jmp convert_end_ctd3


save_name:
        ldi temp, terminator
        st x+, temp
        ldi temp2, 1
        mov r6, temp2
		rjmp convert_end_ctd3

convert_end_ctd2:

		ldi data, 0
		cp r8, data
		breq bbdd
		rcall lcd_backspace
		bbdd:
		inc r8
		mov data, r4
        rcall lcd_wait_busy
        rcall lcd_write_data



convert_end_ctd3:

        mov temp2, r6
        cpi temp2, 1
        brne step_up2
        clr r6
ret

;*****************************************************************************************
; Keypad: End Get Character Input
;*****************************************************************************************
;*****************************************************************************************
; Keypad: Get Numeric Input
;*****************************************************************************************
get_num_input: ;store result in temp ; WENDIS FUNCTION TO GET INTEGER INPUT 1-10
        ldi mask, INITCOLMASK ; initial column mask
        clr col ; initial column

colloop:
        out KEYPORT, mask ; set column to mask value
        ldi temp, 0xFF ; implement delay so hardware can stabilize

keypad_delay:
        dec temp
        brne keypad_delay

wait_for_key:
        in temp, KEYPIN ; read PORTA
        andi temp, ROWMASK ; read only the row bits
        cpi temp, 0xF ; check if any rows are grounded
        breq next_col ; if not go to the next column

wait_key_release:
        in temp2, KEYPIN
        andi temp2, ROWMASK
        cpi temp2, 0xF
        brne wait_key_release

ldi mask, INITROWMASK ; initialise the row check
clr row ; initial row

row_loop:
        mov temp2, temp
        and temp2, mask ; check masked bit
        brne skip_conv ; if the result is non-zero, we need to look again
        rcall convert ; if bit is clear, convert the bitcode
        ret ; go back to where it is called, probably will fail

skip_conv:
        inc row ; else move to the next row
        lsl mask ; shift teh mask to the next bit
        jmp row_loop

next_col:
        cpi col, 3 ; check if we are on the last column
        breq get_num_input ; if so, no buttons were pushed


sec             ; else shift the column mask:
                ; We must set the carry bit
rol mask        ; and then rotate left by a bit,
                ; shifting the carry into
                ; bit zero. We need this to make
                ; sure all the rows have
                ; pull-up resistors
inc col         ; increment column value
jmp colloop ; and check the next column

step_up:
rcall lcd_wait_busy
rcall lcd_write_data
jmp get_num_input

convert:
        cpi col, 3      ; if column is 3 we have a letter
        breq letters
        cpi row, 3      ; if row is 3 we have a symbol or 0
        breq symbols
        mov temp, row   ; otherwise we have a number (1-9)
        lsl temp        ; temp = row * 2
        add temp, row   ; temp = row * 3
        add temp, col   ; add the column address
                        ; to get the offset from 1
        inc temp        ; add 1. Value of switch is
                        ; row*3 + col + 1.
                        ; now print to screen
        ldi data, '0'
        add data, temp
        clr r4
        mov r4, temp    ; store the actual numeric value
        ldi temp, 1     ; store a boolean that one number has already been entered
        mov r5, temp
        mov temp, r4    ; move r4 back into temp in case used again
        jmp convert_end_ctd

letters:
        cpi row, 3      ; check if D was pressed
        breq save_number
        jmp get_num_input   ; if not D, just wait for more input

symbols:
        cpi col, 0 ; check if we have a star
        breq get_num_input
        cpi col, 1 ; or if we have zero
        breq zero
        jmp get_num_input

zero:
        ldi data, '0'
        mov temp, r5
        cpi temp, 1
        breq make_ten
        clr temp
        mov r4, temp
        jmp convert_end_ctd

make_ten:
        ldi temp, 10
        mov r4, temp
        clr r5
        jmp convert_end_ctd

save_number:
        ldi temp2, 1

convert_end_ctd:
		cpi temp2, 1
		brne step_up

        clr temp2
ret
;*****************************************************************************************
; Keypad: End Get Numeric Input
;*****************************************************************************************


;;;;;;;;;
;; LCD ;;
;;;;;;;;;
;****************************************************************************************
; LCD: COM Port Write
;****************************************************************************************
;Function lcd_write_com: Write a command to the LCD. The data reg stores the value to be written.
lcd_write_com:
        out LCD_DATA_PORT, data ; set the data port's value up

        ;clr temp
        in temp, LCD_CONTROL_PORT
        cbr temp,15

        out LCD_CONTROL_PORT, temp ; RS = 0, RW = 0 for a command write
        nop ; delay to meet timing (Set up time)
        sbi LCD_CONTROL_PORT, LCD_E ; turn on the enable pin
        nop ; delay to meet timing (Enable pulse width)
        nop
        nop
        cbi LCD_CONTROL_PORT, LCD_E ; turn off the enable pin
        nop ; delay to meet timing (Enable cycle time)
        nop
        nop
ret
;****************************************************************************************
; LCD: End COM Port Write
;****************************************************************************************
;****************************************************************************************
; LCD: Data Port Write
;****************************************************************************************
;Function lcd_write_data: Write a character to the LCD. The data reg stores the value to be written.
lcd_write_data:
        out LCD_DATA_PORT, data ; set the data port's value up

        in temp, LCD_CONTROL_PORT
        cbr temp, 15
        sbr temp, 1 << LCD_RS

        out LCD_CONTROL_PORT, temp ; RS = 1, RW = 0 for a data write
        nop ; delay to meet timing (Set up time)
        sbi LCD_CONTROL_PORT, LCD_E ; turn on the enable pin
        nop ; delay to meet timing (Enable pulse width)
        nop
        nop
        cbi LCD_CONTROL_PORT, LCD_E ; turn off the enable pin
        nop ; delay to meet timing (Enable cycle time)
        nop
        nop
ret
;****************************************************************************************
; LCD: End Data Port Write
;****************************************************************************************
;****************************************************************************************
; LCD: Wait Busy
;****************************************************************************************
;Function lcd_wait_busy: Read the LCD busy flag until it reads as not busy.
lcd_wait_busy:
        clr temp
        out LCD_DATA_DDR, temp ; Make LCD_DATA_PORT be an input port for now
        out LCD_DATA_PORT, temp

        in temp, LCD_CONTROL_PORT
        cbr temp, 15
        sbr temp, 1 << LCD_RW

        out LCD_CONTROL_PORT, temp ; RS = 0, RW = 1 for a command port read
        busy_loop:
        nop ; delay to meet timing (Set up time / Enable cycle time)
        sbi LCD_CONTROL_PORT, LCD_E ; turn on the enable pin
        nop ; delay to meet timing (Data delay time)
        nop
        nop
        in temp, LCD_DATA_PIN ; read value from LCD
        cbi LCD_CONTROL_PORT, LCD_E ; turn off the enable pin
        sbrc temp, LCD_BF ; if the busy flag is set
        rjmp busy_loop ; repeat command read

        in temp, LCD_CONTROL_PIN
        cbr temp, 15
        ;clr temp ; else

        out LCD_CONTROL_PORT, temp ; turn off read mode,
        ser temp
        out LCD_DATA_DDR, temp ; make LCD_DATA_PORT an output port again
ret ; and return
;****************************************************************************************
; LCD: End Wait Busy
;****************************************************************************************
;****************************************************************************************
; LCD: Delay
;****************************************************************************************
; Function delay: Pass a number in registers r18:r19 to indicate how many microseconds
; must be delayed. Actual delay will be slightly greater (~1.08us*r18:r19). ; r18:r19 are altered in this function.
delay:
        subi del_lo, 1
        sbci del_hi, 0
        nop
        nop
        nop
        nop
        brne delay
ret
;****************************************************************************************
; LCD: End Delay
;****************************************************************************************
;****************************************************************************************
; LCD: Initialisation
;****************************************************************************************
;Function lcd_init Initialisation function for LCD.
lcd_init:
        ser temp
        out LCD_DATA_DDR, temp ; LCD_DATA_PORT, the data port is usually all otuputs
        out LCD_CONTROL_DDR, temp ; LCD_CONTROL_PORT, the control port is always all outputs
        delay_macro 15000 ; delay for > 15ms
        ; Function set command with N = 1 and F = 0
        ldi data, LCD_FUNC_SET | (1 << LCD_N)
        rcall lcd_write_com ; 1st Function set command with 2 lines and 5*7 font
        delay_macro 4100 ; delay for > 4.1ms
        rcall lcd_write_com ; 2nd Function set command with 2 lines and 5*7 font
        delay_macro 100; delay for > 100us
        rcall lcd_write_com ; 3rd Function set command with 2 lines and 5*7 font
        rcall lcd_write_com ; Final Function set command with 2 lines and 5*7 font
        rcall lcd_wait_busy ; Wait until the LCD is ready
        ldi data, LCD_DISP_OFF
        rcall lcd_write_com ; Turn Display off
        rcall lcd_wait_busy ; Wait until the LCD is ready
        ldi data, LCD_DISP_CLR
        rcall lcd_write_com ; Clear Display
        rcall lcd_wait_busy ; Wait until the LCD is ready
        ; Entry set command with I/D = 1 and S = 0
        ldi data, LCD_ENTRY_SET | (1 << LCD_ID)
        rcall lcd_write_com ; Set Entry mode: Increment = yes and Shift = no
        rcall lcd_wait_busy ; Wait until the LCD is ready
        ; Display on command with C = 0 and B = 1
        ldi data, LCD_DISP_ON | (1 << LCD_C)
        rcall lcd_write_com ; Turn Display on with a cursor that doesn't blink
ret
;****************************************************************************************
; LCD: End Initialisation
;****************************************************************************************
;****************************************************************************************
; LCD: Custom Functions
;****************************************************************************************
lcd_goto_line2:
        push data
        ldi data, LCD_ADDR_SET | LCD_LINE2
        rcall lcd_wait_busy
        rcall lcd_write_com
        pop data
ret

lcd_print_data:
        rcall lcd_wait_busy
        rcall lcd_write_data
ret

lcd_clear_screen:
        push data
        ldi data, LCD_DISP_CLR
        rcall lcd_wait_busy
        rcall lcd_write_com
        pop data
ret
lcd_backspace:
        push data
		ldi data, LCD_BACK
		rcall lcd_wait_busy
        rcall lcd_write_com
 pop data
ret

regIntToLCD: ;assumes in data
	push temp
	push r12

		cpi temp, 10
		brlt onedigit

		clr r12
		inc r12
		endtenloop:
		intToLCD r12
		clr r12
		intToLCD r12

		rjmp endprintreg

		onedigit:
		intToLCD temp

		endprintreg:
	pop r12
	pop temp
ret

lcd_psfm:
        lpm data, z+                    ; read a character from the string
        cpi data, terminator
        breq end_lcd_psfm
        rcall lcd_print_data    ; write the character to the screen
        brne lcd_psfm                  ; loop again if there are more characters
end_lcd_psfm:
ret

lcd_psfdm:
        ld data, z+                    ; read a character from the string
        cpi data, terminator
        breq end_lcd_psfdm
        rcall lcd_print_data    ; write the character to the screen
        brne lcd_psfdm                  ; loop again if there are more characters
end_lcd_psfdm:
ret

;****************************************************************************************
; LCD: End Custom Functions
;****************************************************************************************