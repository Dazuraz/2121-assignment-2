\contentsline {section}{\numberline {1}About the emulator}{1}
\contentsline {subsection}{\numberline {1.1}Introduction}{1}
\contentsline {subsection}{\numberline {1.2}The Monorail Environment}{1}
\contentsline {subsection}{\numberline {1.3}Monorail Configuration Constraints}{1}
\contentsline {section}{\numberline {2}Pin Configuration}{1}
\contentsline {section}{\numberline {3}Controls}{3}
\contentsline {subsection}{\numberline {3.1}Keypad: Alphabetical Mode}{3}
\contentsline {subsection}{\numberline {3.2}Keypad: Numerical Mode}{3}
\contentsline {subsection}{\numberline {3.3}Unique Keys}{3}
\contentsline {section}{\numberline {4}Displays}{4}
\contentsline {subsection}{\numberline {4.1}During monorail configuration:}{4}
\contentsline {subsection}{\numberline {4.2}After monorail configuration:}{4}
